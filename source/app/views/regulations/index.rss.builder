xml.instruct! :xml, :version => "1.0" 
xml.rss :version => "2.0" do
  xml.channel do
    xml.title s_t(Regulation,:last_issued)
    xml.description s_t(Regulation,:index,:actions)+" "+Sysconfig.current.company
    xml.link regulations_url

    @regulations_for_feed.each do |regulation|
      xml.item do
        xml.title regulation.title+": "+regulation.description[0..50].strip+"..."
        xml.description regulation.description
        xml.pubDate regulation.date.to_s(:rfc822)
        xml.link regulation_url(regulation)
        xml.guid regulation_url(regulation)
      end
    end
  end
end
