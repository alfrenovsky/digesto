# encoding: utf-8

class PdfUploader < CarrierWave::Uploader::Base

  def cache_dir
    "#{RAILS_ROOT}/tmp/uploads"
  end

  # Include RMagick or ImageScience support:
  include CarrierWave::RMagick
  include CarrierWave::MimeTypes
  # include CarrierWave::ImageScience

  # Choose what kind of storage to use for this uploader:
  storage :file
  # storage :fog

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "../uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  # Provide a default URL as a default if there hasn't been a file uploaded:
  # def default_url
  #   "/images/fallback/" + [version_name, "default.png"].compact.join('_')
  # end

  # Process files as they are uploaded:
  # process :resize => [200, 300]
  #
  # def resize(width, height)
  #   # do something
  # end

  # Create different versions of your uploaded files:

  version :cover do
    process :cover
    process :resize_to_fit => [768,1024]
    process :convert => :png
    def full_filename(for_file=nil)
      "cover.png"
    end
  end

  version :thumb do
    process :cover
    process :resize_to_fit => [90,120]
    process :convert => :png
    def full_filename(for_file=nil)
      "thumb.png"
    end
  end
  version :icon do
    process :cover
    process :resize_to_fit => [36,48]
    process :convert => :png
    def full_filename(for_file=nil)
      "icon.png"
    end
  end

  version :text do
    process :pdftotext
    def full_filename(for_file=nil)
      "text.txt"
    end
  end

  def pdftotext
    text = `pdftotext -layout #{current_path} -` || ""
    aFile = File.new(current_path, "w")
    aFile.write(text)
    aFile.close
  end

  def cover
    i=Magick::Image.read("#{current_path}[0]"){self.density="288"}.first
    i.density="72"
    i.write("#{current_path}")
  end 

  def extension_white_list
     %w(pdf)
  end

  # Override the filename of the uploaded files:
  # Avoid using model.id or version_name here, see uploader/store.rb for details.
  def filename
     "original.pdf" if original_filename
  end

end
