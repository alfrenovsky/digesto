# encoding: UTF-8

module ApplicationHelper

  # rud = crud-c
  def rud_links(object)
    model_name=object.class.model_name.underscore
    output = []
    output << link_to_unless_current("<span>#{s_t(nil,:show,:action)}</span>".html_safe, object, :class => "show") do link_back end if can? :show, object 
    output << link_to("<span>#{s_t(nil,:edit,:action)}</span>".html_safe, send("edit_#{model_name}_path",object), :class => "edit" ) if can? :edit, object
    output << link_to("<span>#{s_t(nil,:destroy,:action)}</span>".html_safe, object, :confirm => s_t(object,:confirm,:action), :method => :delete, :class => "destroy" ) if can? :destroy, object
    output.join(" ").html_safe
  end

  def link_back
    link_to "<span>#{s_t(nil,:back,:action)}</span>".html_safe, :back, :class => "back"
  end

    
  def create_link(klass)
    model_name=klass.model_name.underscore
    output = []
    output << link_to("<span>#{s_t(klass,:new,:action)}</span>".html_safe, send("new_#{model_name}_path"), :class => "new" ) if can? :create, klass
    output.join(" ").html_safe
  end

	def link_to_remove_fields(name, f)
		f.hidden_field(:_destroy)+link_to_function(name, "remove_fields(this)", :class => "destroy")
	end

	def link_to_add_fields(name, f, options)
		options={} if options.nil?
		field_name=options[:field_name]
		model_name= f.object.class.reflect_on_association(field_name).class_name.underscore
		partial_model_name=options[:partial_model] && options[:partial_model].to_s.downcase.underscore || model_name
		new_object = f.object.class.reflect_on_association(field_name).klass.new
		fields = f.fields_for(field_name, new_object, :child_index => "new_#{field_name}") do |builder|
			render(partial_model_name.pluralize+"/"+partial_model_name + "_field", :f => builder)
		end
		link_to_function(name, "add_fields(this, \"#{field_name}\", \"#{escape_javascript(fields)}\")", :class => "new")
	end

	def flash_div
		flashed=false
		title={:alert => "!", :notice => "^", :error => "x"}
		output=""
		output << '<div class="flash"><table><tr><td>'
		flash.each_pair do |key,msg| 
			tit=content_tag(:div, title[key], :class => :title).html_safe
			cont=content_tag(:div, msg, :class => :content).html_safe
			output << content_tag(:div, tit+cont, :class => "message #{key.to_s}")
			flashed=true
		end 
		output << "</td></tr></table></div>"
		output.html_safe if flashed
	end


  def file_url(object,field_name,version=nil,disposition=:inline)
    if object.send(field_name.to_s+"?")
      version = version.to_s unless version.nil?
      field=object.send(field_name.to_s)
      if object.id
        view_path( :class_name => object.class.name.underscore, :field_name => field_name.to_s, :version => version,:id => object.id, :disposition => disposition.to_s )
      else
        view_temp_path(Pathname.new(field.url(version).to_s).realpath.relative_path_from(Pathname.new(field.cache_dir).realpath)) rescue nil
      end
    end
  end

end
