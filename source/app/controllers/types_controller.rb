class TypesController < ApplicationController
  load_and_authorize_resource
  def index
  end

  def show
  end

  def new
  end

  def edit
  end

  def create
    if @type.save
      redirect_back_or_to types_path
    else
      render :action => "new" 
    end
  end

  def update
    if @type.update_attributes(params[:type])
      redirect_back_or_to types_path
    else
      render :action => "edit" 
    end
  end

  def destroy
    @type.destroy
    redirect_back_or_to types_path
  end

  def detectors_index
    @types=Type.scoped
  end

end
