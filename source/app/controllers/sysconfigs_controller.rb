class SysconfigsController < ApplicationController
  authorize_resource
  def edit
    @sysconfig = Sysconfig.current
  end

  def update
    @sysconfig = Sysconfig.current

    if @sysconfig.update_attributes(params[:sysconfig])
      redirect_to edit_config_path
    else
      render :action => "edit"
    end
  end
end
