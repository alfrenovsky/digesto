class InstitutionsController < ApplicationController
  load_and_authorize_resource

  def index
  end

  def show
  end

  def new
  end

  def edit
  end

  def create
    if @institution.save
      redirect_back_or_to institutions_path 
    else
      render :action => "new"
    end
  end

  def update
    if @institution.update_attributes(params[:institution])
      redirect_back_or_to institutions_path 
    else
      render :action => "edit" 
    end
  end

  def destroy
    @institution.destroy
    redirect_back_or_to institutions_path 
  end
end
