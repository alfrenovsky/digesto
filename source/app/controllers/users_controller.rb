class UsersController < ApplicationController
  load_and_authorize_resource
  def index
  end

  def show
  end

  def new
    @user = User.new
  end

  def edit
  end

  def create
    @user = User.new(params[:user])
    if @user.save
      redirect_back_or_to users_path
    else
      render :action => "new"
    end
  end

  def update
    @user = User.find(params[:id], :readonly => false)
    if @user.update_attributes(params[:user])
      redirect_back_or_to users_path
    else
      render :action => "edit" 
    end
  end

  def destroy
    @user.destroy
    redirect_back_or_to users_path
  end
end
