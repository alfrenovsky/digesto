class ExpressionsController < ApplicationController
  load_and_authorize_resource

  def index
    @expressions=Expression.orphans
  end

  def show
  end

  def new
  end

  def edit
  end

  def create
    if @expression.save
      redirect_back_or_to expressions_path
    else
      render :action => "new" 
    end
  end

  def update
    if @expression.update_attributes(params[:expression])
      redirect_back_or_to expressions_path
    else
      render :action => "edit" 
    end
  end

  def destroy
    @expression.destroy
    redirect_back_or_to expressions_path
  end

end
