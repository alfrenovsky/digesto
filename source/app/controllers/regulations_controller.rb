class RegulationsController < ApplicationController
  load_and_authorize_resource

  def index
    @regulations=Regulation.includes(:issuer).includes(:type).includes(:attachments).includes(:categories)
    if can? :edit, Regulation
      @regulations=@regulations
    else
      @regulations=@regulations.confirmed
    end
    @regulations=@regulations.where("date >= ? and date <= ?","#{params[:search_year]}0101","#{params[:search_year]}1231") unless params[:search_year].blank?  
    @regulations=@regulations.where(:types => { :id => params[:search_type]}) unless params[:search_type].blank?

    unless params[:search_category].blank?
      if params[:search_category] == "-" then
        @regulations=@regulations.where(:has_categories => false)
      else
        @regulations=@regulations.where(:categories => { :id => params[:search_category]})
      end
    end

    @regulations=@regulations.where(:issuers => { :id => params[:search_issuer]}) unless params[:search_issuer].blank?
    @regulations=@regulations.where(:number => params[:search_number]) unless params[:search_number].blank?
		@regulations=@regulations.order("confirmation").order(params[:order] != "updated_at" ? "regulations.date DESC" : "regulations.updated_at DESC").order("number DESC")
    @regulations_for_feed=@regulations.limit(50)

    search=params[:search]
    unless search.blank?
      search.gsub!("\"","")
      searchable = []
      searchable += search.mb_downcase.to_ascii.scan(/\w+/)
      searchable += search.mb_downcase.to_ascii.scan(/[0-9\.]+/)

      ids_ary=searchable.map{ |word_word| (Word.where(:word => word_word).joins(:indices).first || Word.new).regulation_ids}
      regulation_ids=ids_ary.shift
      ids_ary.each{|ids| regulation_ids &= ids}
      @regulations=@regulations.where(:id => regulation_ids)
    end

    @regulations_count=@regulations.size
		@regulations=@regulations.paginate(:per_page => 7, :page => params[:page])
  end

  def show
  end

  def new
  end

  def edit
  end

  def create
    if @regulation.save
      redirect_back_or_to regulations_path
    else
      render :action => "new"
    end
  end

  def mass_create
    ajax_upload = params[:qqfile].is_a?(String)
    filename = ajax_upload  ? params[:qqfile] : params[:qqfile].original_filename
    extension = filename.split('.').last
    # Creating a temp file
    tmp_file=""
    while tmp_file.blank? || File.exists?(tmp_file) do
      tmp_file = "#{Rails.root}/tmp/uploaded.#{Time.now.to_f.to_s}.#{extension}"
    end
    # Save to temp file
    File.open(tmp_file, 'wb') do |f|
      if ajax_upload
        f.write  request.body.read
      else
        f.write params[:qqfile].read
      end
    end
    @regulation=Regulation.new
    @regulation.file = File.open(tmp_file)
    @regulation.infer_missing_data
    @result=@regulation.save(:validate => false) ? { :success => true } : "error"
    respond_to do |format|
       format.js { render :json => @result.to_json }
    end
  end

  def update
    @regulation=Regulation.find params[:id], :readonly => false
    if @regulation.update_attributes(params[:regulation])
      redirect_back_or_to regulations_path
    else
      render :action => "edit" 
    end
  end

  def destroy
    @regulation.destroy
    redirect_to regulations_path
  end

  def missing
    @missing={}
    regulations=Regulation.joins(:issuer,:type).order(:date)
    regulations=regulations.group_by{|r| "#{r.type.abbreviation}-#{r.issuer.abbreviation}/#{r.date.year}" rescue nil}

    regulations.each do |k,v|
      v=v.map{|r| r.number rescue nil}.compact.sort.uniq
      last=v.last
      missing=(1..last).map{|n| n unless v.include? n}.compact

      intervals=[[missing.first,missing.first]]

      missing.each do |m|
        if intervals.last.last+1 >= m
          intervals.last[1]=m
        else
          intervals << [m,m]
        end
      end

      @missing[k]=intervals.map{|i| i.first == i.last ? i.first.to_s : i.first.to_s+"-"+i.last.to_s} unless k.blank?

    end
  end

end
