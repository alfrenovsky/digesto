class RegulationTypesController < ApplicationController
  load_and_authorize_resource
  def index
    @regulation_types=RegulationType.includes(:expressions)
  end

  def show
  end

  def new
    @regulation_type = RegulationType.new
    @regulation_type.build_expression unless @regulation_type.expression
  end

  def edit
  end

  def create
    @regulation_type = RegulationType.new(params[:regulation_type])
    if @regulation_type.save
      redirect_back_or_to regulation_types_path
    else
      render :action => "new" 
    end
  end

  def update
    @regulation_type = RegulationType.find(params[:id], :readonly => false)
    if @regulation_type.update_attributes(params[:regulation_type])
      redirect_back_or_to regulation_types_path
    else
      render :action => "edit" 
    end
  end

  def destroy
    @regulation_type.destroy
    redirect_back_or_to regulation_types_path
  end
end
