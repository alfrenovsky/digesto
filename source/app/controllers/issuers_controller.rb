class IssuersController < ApplicationController
  load_and_authorize_resource
  def index
  end

  def show
  end

  def new
  end

  def edit
  end

  def create
    if @issuer.save
      redirect_back_or_to issuers_path
    else
      render :action => "new" 
    end
  end

  def update
    if @issuer.update_attributes(params[:issuer])
      redirect_back_or_to issuers_path
    else
      render :action => "edit" 
    end
  end

  def destroy
    @issuer.destroy
    redirect_back_or_to issuers_path
  end
end
