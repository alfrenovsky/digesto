class WordsController < ApplicationController
  load_and_authorize_resource
  def index
    @words=Word.all.sort{|x,y| y.regulations_count <=> x.regulations_count }
  end
end
