class FileController < ApplicationController
  def file
    if params[:tmp_file]
      # /view/tmp/*tmp_file
    else
      # /view/:class_name/:field_name/:id(/:version)(.:format)
      disposition=params[:disposition]
      class_name = (["regulation","attachment"].include?(params[:class_name]) ? params[:class_name] : nil)
      klass = eval(class_name.camelize) 
      object=klass.find(params[:id])
      field=object.send(params[:field_name])
      version=params[:version]
      file=version ? field.versions[version.to_sym] : field
      mime_type=file.set_content_type
      extension=Mime::LOOKUP[mime_type].instance_variable_get("@symbol").to_s
      extension ||= Mime::LOOKUP[mime_type].instance_variable_get("@string")[/[^\/]*$/].to_s.strip
      extension='pdf'

      File.open(file.path, 'r') do |f|
        send_data f.read, :disposition => "#{disposition}; filename=#{object.try(:title)}.#{extension}", :type=> mime_type
      end
    end
  end
end
