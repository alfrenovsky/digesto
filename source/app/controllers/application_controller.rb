# encoding: UTF-8

class ApplicationController < ActionController::Base
	protect_from_forgery

	include BackToRefererAfterForm

	rescue_from CanCan::AccessDenied do |exception|
		#flash[:error] = s_t("ability", :access_denied)
		redirect_to :back rescue redirect_to :root
	end

  before_filter :set_mailer_host, :ensure_sysconfig_present

  def set_mailer_host
    ActionMailer::Base.default_url_options = {:host => request.host_with_port}
    ActionMailer::Base.delivery_method = :smtp
    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.raise_delivery_errors = false
    ActionMailer::Base.smtp_settings = {
      :address            => Sysconfig.current.smtp_address,
      :port               => Sysconfig.current.smtp_port,
      :domain             => Sysconfig.current.smtp_domain,
      :authentication     => Sysconfig.current.smtp_authentication == "none" ? nil : Sysconfig.current.smtp_authentication.to_sym ,
      :user_name          => Sysconfig.current.smtp_user_name,
      :password           => Sysconfig.current.smtp_password
    } rescue nil
    Devise.setup do |config|
      config.mailer_sender = Sysconfig.current.default_sender
    end
  end

  def ensure_sysconfig_present
    if Sysconfig.current.nil?
      Sysconfig.new.save :validate => false
      redirect_to :controller => :sysconfigs, :action => :edit
    end
  end
end
