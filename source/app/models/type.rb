class Type < ActiveRecord::Base
  has_many :regulation_types, :dependent => :destroy
  has_many :issuers, :through => :regulation_types
  validates_presence_of :name, :abbreviation
  validates_length_of :abbreviation, :is => 3
  validates_uniqueness_of :name, :abbreviation

  has_many :expressions, :as => :regexable
  accepts_nested_attributes_for :expressions, :allow_destroy => true

  before_validation :capitalize_abbr
  def capitalize_abbr
    self.abbreviation.upcase!
  end
end
