class Issuer < ActiveRecord::Base

  extend FriendlyId
  friendly_id :name

  default_scope order(:name)
	belongs_to :institution
  validates_presence_of :name, :abbreviation, :institution
  validates_length_of :abbreviation, :is => 2
  validates_uniqueness_of :name, :abbreviation


  has_many :regulation_types, :dependent => :destroy
  has_many :types, :through => :regulation_types

  before_validation :capitalize_abbr
  def capitalize_abbr
    self.abbreviation.upcase!
  end

  def display_name
    [name,institution.name].join " "
  end

end
