class Attachment < ActiveRecord::Base
  validates_presence_of :name, :file
  belongs_to :regulation
  mount_uploader :file, PdfUploader

  def title
    regulation.title+"_"+name
  end
end
