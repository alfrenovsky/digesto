class Word < ActiveRecord::Base
  has_many :indices, :dependent => :destroy
  has_many :regulations, :through => :indices

  def regulations_count
    @regulations_count ||= regulations.count
  end
end
