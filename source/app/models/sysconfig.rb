class Sysconfig < ActiveRecord::Base

  validates_presence_of :company, :default_sender, :smtp_address, :smtp_port, :smtp_domain, :smtp_authentication
  validates_numericality_of :smtp_port, :greater_than_or_equal_to => 1, :less_than_or_equal_to => 65535 

  AUTH_TYPES=[ :none, :plain, :login, :cram_md5 ]

  def self.current
    @sysconfig ||= Sysconfig.first || Sysconfig.new
  end
  mount_uploader :logo, LogoUploader
end
