class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable, :lockable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :role_ids
  has_many :permissions
  has_many :roles, :through => :permissions
  accepts_nested_attributes_for :roles

  def is?(role)
    @is ||= {}
    @is[role] ||=  self.roles.map{|r| r.name}.include?(role.to_s) || User.joins(:roles).where( :roles => {:name => "admin"}).first.nil?
  end

end
