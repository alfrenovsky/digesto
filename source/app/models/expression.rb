class Expression < ActiveRecord::Base

  scope :orphans,  where(:regexable_id => nil ).order(:regexable_type, :regex)

	belongs_to :regexable, :polymorphic => true
  after_commit :reinfer

  def computed_regex
    self.as_string? ? Regexp.escape(regex) : regex
  end

  def self.type(sym)
    where(:regexable_type => sym.to_s)
  end
    
  TYPES=[ :cleaner, :paragraph_starter, :paragraph_finisher, :description_detector, :date_detector ].map{|t| [ s_t(Expression, t), t.to_s ] }
 


  def reinfer
    Regulation.where(:confirmation => false).each do |reg|
      if reg.outdated?
        reg.infer_missing_data(:force => true)
        reg.save(:validate => false)
      end
    end
  end

end
