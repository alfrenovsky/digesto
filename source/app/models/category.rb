class Category < ActiveRecord::Base
  has_many :regulation_category, :dependent => :destroy
  has_many :regulations, :through => :regulation_category
end
