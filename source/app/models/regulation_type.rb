class RegulationType < ActiveRecord::Base
  default_scope joins(:issuer).joins(:type).order("issuers.name","types.name")
  belongs_to :issuer
  belongs_to :type
  has_many :regulations, :dependent => :restrict

  has_many :expressions, :as => :regexable
  accepts_nested_attributes_for :expressions, :allow_destroy => true

  def display_name
    [self.type.name,self.issuer.name,self.issuer.institution.name].join " "
  end
end
