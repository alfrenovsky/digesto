class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    user ||= User.new # guest user (not logged in)

    can :manage, :all if user.is?(:admin) 
    cannot :show, :all
    #
    # The first argument to `can` is the action you are giving the user permission to do.
    # If you pass :manage it will apply to every action. Other common actions here are
    # :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on. If you pass
    # :all it will apply to every resource. Otherwise pass a Ruby class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details: https://github.com/ryanb/cancan/wiki/Defining-Abilities
    cannot :destroy, Institution do |i|
      i.issuers.count != 0
    end
    cannot :destroy, RegulationType
    cannot :create, RegulationType
    cannot :create, User
    cannot :destroy, User do |u|
      u == user
    end

    can :index, Regulation
    can :show, Regulation
    can :file, Regulation # file is the real action for view and download

    can :manage, Regulation if user.is?(:operator) 
  end
end
