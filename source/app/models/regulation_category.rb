class RegulationCategory < ActiveRecord::Base
  belongs_to :regulation
  belongs_to :category
end
