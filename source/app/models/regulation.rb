# encoding: UTF-8

class Regulation < ActiveRecord::Base

  scope :confirmed, where(:confirmation => true)

  has_many :indices, :dependent => :destroy
  has_many :words, :through => :indices

  has_many :regulation_category, :dependent => :destroy
  has_many :categories, :through => :regulation_category

  belongs_to :regulation_type
  has_one :type, :through => :regulation_type
  has_one :issuer, :through => :regulation_type
  #validates_presence_of :number, :date, :regulation_type, :description
  validates_presence_of :number, :date, :regulation_type
  validates_numericality_of :number
  validates_acceptance_of :confirmation, :accept => true

  has_many :attachments
  accepts_nested_attributes_for :attachments, :allow_destroy => true
  accepts_nested_attributes_for :categories, :allow_destroy => true

  MONTHS={ 
    :ene => 1,
    :feb => 2,
    :mar => 3,
    :abr => 4,
    :may => 5,
    :jun => 6,
    :jul => 7,
    :ago => 8,
    :sep => 9,
    :set => 9,
    :oct => 10,
    :nov => 11,
    :dic => 12,
  }


  attr_protected :text_content

  mount_uploader :file, PdfUploader

  def self.years_range
    #start=Regulation.order(:date).where("date IS NOT NULL").first.date rescue Date.today
    #finnish=Regulation.order(:date).where("date IS NOT NULL").last.date rescue Date.today
    #(start.year..finnish.year).to_a
    Regulation.select(:date).all.map{|r| r.date.year rescue nil}.compact.uniq.sort
  end

  def title
    [issuer.abbreviation, date.year, type.abbreviation, ("000"+number.to_s)[-3..-1]].join "-"  rescue id.to_s
  end

  def full_title
    [type.name, issuer.name,"-","Nro.",("000"+number.to_s)[-3..-1],"-","Año",date.year].join " "  rescue id.to_s
  end

  def description_to_show
    description.blank? ? full_title : description 
  end

  before_validation :infer_missing_data
  before_save :update_index_if_needed

  def text_content
    @text_content ||= extract_data_from_pdf || ""
  end

  def outdated?
    (not confirmation) and (Expression.order(:updated_at).last.updated_at > updated_at)
  end

  def infer_missing_data(*params)
    options=params.extract_options!
    force=options[:force]
    infer_abstract if abstract.blank? or force
    infer_date if date.nil? or force
    infer_regulation_type if regulation_type_id.nil? or force
    infer_regulation_number if number.nil? or force
    unless self.confirmation
      infer_description if description.blank? or force
    end
  end

  def update_index_if_needed
    has_categories=(categories.count != 0 ? true : false)
    if self.changed.include?("abstract") or self.changed.include?("description") or self.words.count == 0
      self.update_index
    end
  end
  def update_index
      text=(self.abstract.to_s+" "+self.description.to_s).mb_downcase.to_ascii
      #text.gsub!(/[^\w \n]*/,"")
      indexable = []
      indexable += text.scan(/\w+/)
      indexable += text.scan(/[0-9\.]+/)
      self.words=indexable.uniq.map{|w| Word.where(:word => w).first || Word.new(:word => w)}
  end

  private

  def extract_data_from_pdf
    if file?
      text = ""
      File.open(self.file.url(:text), 'r') do |f|
        text = f.read
      end rescue ""
      text ||= "" 
      # text = `pdftotext -layout #{file.path} -` || ""

      # text.encode!('utf-8')

      text.gsub!("\f","\n\n") # Salto de pagina a salto de parrafo
      text.gsub!(/\ +/," ") # Elimina espacios dobles
      text.gsub!(/\ *\n\ */,"\n") # Elimina espacios al final y principio de linea

      Expression.type(:paragraph_starter).all.each do |s|
          text.gsub!(/\ *(#{s.computed_regex})/,"\n\n"+'\1')
      end
      Expression.type(:paragraph_finisher).all.each do |f|
          text.gsub!(/(#{f.computed_regex})\ */,'\1'+"\n\n")
      end

      text.gsub!(/-\n/,"") # Elimina separacion en silabas
      text.gsub!(/\n{2,}/,"\302\205").gsub!("\n"," ") rescue nil # convierte los dobles (o mas) saltos de linea en salto de parrafo

      text.gsub!(/(\302\205)+/,"\n\n") # convierte los saltos de parrafo a doble salto de linea

      Expression.type(:cleaner).all.each do |cs|
        text.gsub!(/#{cs.computed_regex}/,"")
      end

      text.gsub!(/\n\s*\n/,"\n\n") # Si despues de la eliminacion quedan tres o mas saltos (o saltos con espacios) los dejo en dos

      text.strip
    end
  end rescue nil

  def infer_abstract
    self.abstract=text_content 
    self.confirmation=false unless self.abstract.blank?
  end

  def infer_description
    desc=nil
    Expression.type(:description_detector).all.each do |dd|
      desc ||= self.abstract.scan(/#{dd.computed_regex}/).flatten.first.to_s unless self.abstract.nil?
    end
    self.description=desc
    #self.confirmation=false unless desc.nil?
  end

  def infer_date
    idate=nil
    Expression.type(:date_detector).all.each do |dd|
      idate ||= text_content.scan(/#{dd.computed_regex}/).first rescue nil
    end
    if idate 
      day=idate[0].to_i
      month=MONTHS[idate[1][0..2].mb_downcase.to_sym].to_i
      year=idate[2].to_i
      cdate=Date.civil(year,month,day) rescue nil
      self.date=cdate unless cdate.nil?
    end rescue nil
    self.confirmation=false unless self.date.nil?
  end

  def infer_regulation_type
    RegulationType.joins(:expressions).includes(:expressions).each do |rt|
      rt.expressions.each do |e|
        self.regulation_type_id=rt.id if text_content[/#{e.computed_regex}/]
      end
    end
    self.confirmation=false unless self.regulation_type_id.nil?
  end

  def infer_regulation_number
    Type.joins(:expressions).each do |t|
      t.expressions.each do |e|
        detected=text_content[/#{e.computed_regex}\s*\d+/][/\d+/].to_i rescue nil
          if detected
            self.number=detected
          end
      end
    end
    self.confirmation=false unless self.number.blank?
  end

end
