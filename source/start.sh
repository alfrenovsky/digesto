#!/bin/bash
rake db:create
rake db:migrate
bundle exec unicorn -E production -c config/unicorn.rb
