# encoding: UTF-8

def s_t(object= nil, attribute= :model, type= :labels )
  model_class = (object.class == Class) ? object : object.class 
  model_name = model_class.model_name.underscore rescue ''
  attribute_name = attribute.to_s.downcase
  action_name = self.request.filtered_parameters["action"].downcase rescue ''
  type=type.to_s.downcase.pluralize

  attribute_count = attribute_name.singularize == attribute_name ? 1 : 3
  attribute_as_model = I18n.translate attribute_name.singularize, :scope => "activerecord.models", :count => attribute_count, :default => [""]

  defaults = ::Formtastic::I18n::SCOPES.map do |i18n_scope|
    i18n_path = "formtastic.%{type}.#{i18n_scope}"
    i18n_path.gsub!('%{action}', action_name)
    i18n_path.gsub!('%{model}', model_name)
    i18n_path.gsub!('%{nested_model}', "") # don't how to get nested_mode
    i18n_path.gsub!('%{type}', type)
    i18n_path.gsub!('%{attribute}', attribute_name)
    i18n_path.gsub!(/\.+/, '.')
    i18n_path.to_sym
  end

  defaults << attribute_as_model unless attribute_as_model.blank?

  defaults.uniq!


  options={}
  if object.nil?
    options[:model]=""
    options[:collection]=""
  else
    options[:model]=model_class.model_name.human
    options[:collection]=I18n.translate model_name, :scope => "activerecord.models", :count => 3
  end

  return options[:model] if attribute_name == "model"
  return options[:collection] if attribute_name == "collection"

  I18n.translate(defaults.first, options.merge(:default => defaults)).strip
end

