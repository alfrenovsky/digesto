# encoding: UTF-8

class String
  def mb_upcase
    self.mb_chars.upcase.to_s
  end
  def mb_downcase
    self.mb_chars.downcase.to_s
  end
  def to_ascii
    self.tr "áàâäéèêëíìîïóòôöúùûüñ","aaaaeeeeiiiioooouuuun"
  end
end
