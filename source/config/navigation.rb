# Configures your navigation
SimpleNavigation::Configuration.run do |navigation|
  # Specify a custom renderer if needed.
  # The default renderer is SimpleNavigation::Renderer::List which renders HTML lists.
  # The renderer can also be specified as option in the render_navigation call.
  # navigation.renderer = Your::Custom::Renderer

  # Specify the class that will be applied to active navigation items. Defaults to 'selected'
  # navigation.selected_class = 'your_selected_class'

  # Item keys are normally added to list items as id.
  # This setting turns that off
  # navigation.autogenerate_item_ids = false

  # You can override the default logic that is used to autogenerate the item ids.
  # To do this, define a Proc which takes the key of the current item as argument.
  # The example below would add a prefix to each key.
  # navigation.id_generator = Proc.new {|key| "my-prefix-#{key}"}

  # If you need to add custom html around item names, you can define a proc that will be called with the name you pass in to the navigation.
  # The example below shows how to wrap items spans.
  # navigation.name_generator = Proc.new {|name| "<span>#{name}</span>"}

  # The auto highlight feature is turned on by default.
  # This turns it off globally (for the whole plugin)
  # navigation.auto_highlight = false

  # Define the primary navigation
  navigation.items do |primary|
    # Add an item to the primary navigation. The following params apply:
    # key - a symbol which uniquely defines your navigation item in the scope of the primary_navigation
    # name - will be displayed in the rendered navigation. This can also be a call to your I18n-framework.
    # url - the address that the generated item links to. You can also use url_helpers (named routes, restful routes helper, url_for etc.)
    # options - can be used to specify attributes that will be included in the rendered navigation item (e.g. id, class etc.)
    #           some special options that can be set:
    #           :if - Specifies a proc to call to determine if the item should
    #                 be rendered (e.g. <tt>:if => Proc.new { current_user.admin? }</tt>). The
    #                 proc should evaluate to a true or false value and is evaluated in the context of the view.
    #           :unless - Specifies a proc to call to determine if the item should not
    #                     be rendered (e.g. <tt>:unless => Proc.new { current_user.admin? }</tt>). The
    #                     proc should evaluate to a true or false value and is evaluated in the context of the view.
    #           :method - Specifies the http-method for the generated link - default is :get.
    #rge             :highlights_on - if autohighlighting is turned off and/or you want to explicitly specify
    #                            when the item should be highlighted, you can set a regexes which is matched
    #                            against the current URI.
    #
    primary.item :regulations, s_t(Regulation,:index, :actions), regulations_path, :highlights_on => /^#{regulations_path}/ if can? :index, Regulation 
    if can? :edit, Sysconfig
      primary.item :admin, s_t(Regulation, :admin), edit_config_path do |admin|
        admin.item :conf, s_t(Sysconfig), edit_config_path if can? :edit, Sysconfig
        admin.item :users, s_t(User,:collection), users_path if can? :index, User
        admin.item :institutions, s_t(Institution,:collection), institutions_path, :highlights_on => /^#{institutions_path}/ if can? :index, Institution
        admin.item :issuers, s_t(Issuer,:collection), issuers_path, :highlights_on => /^#{issuers_path}/ if can? :index, Issuer
        admin.item :types, s_t(Type,:collection), types_path, :highlights_on => /^#{types_path}/ if can? :index, Type
        admin.item :regul_type, s_t(RegulationType, :collection), regulation_types_path, :highlights_on => /^#{regulation_types_path}/ if can? :index, RegulationType
        admin.item :categories, s_t(Category, :collection), categories_path, :highlights_on => /^#{categories_path}/ if can? :index, Category
        admin.item :expressions, s_t(Expression, :collection), expressions_path, :highlights_on => /^#{expressions_path}/ if can? :index, Expression
        admin.item :backup, s_t(Sysconfig, :backup), backups_path, :highlight_on => /^#{backups_path}/ if can? :index, :backup
        end
    end
    primary.item :sign_in, s_t(User,:sign_in,:action), new_user_session_path unless user_signed_in?
    primary.item :sign_out, s_t(User,:sign_out,:action), destroy_user_session_path if user_signed_in?
    primary.item :profile, s_t(User,:edit_profile, :action), edit_user_registration_path if user_signed_in?

    # Add an item which has a sub navigation (same params, but with block)
    #primary.item :key_2, 'name', url, options do |sub_nav|
      # Add an item to the sub navigation (same params again)
    #  sub_nav.item :key_2_1, 'name', url, options
    #end

    # You can also specify a condition-proc that needs to be fullfilled to display an item.
    # Conditions are part of the options. They are evaluated in the context of the views,
    # thus you can use all the methods and vars you have available in the views.
    #primary.item :key_3, 'Admin', url, :class => 'special', :if => Proc.new { current_user.admin? }
    #primary.item :key_4, 'Account', url, :unless => Proc.new { logged_in? }

    # you can also specify a css id or class to attach to this particular level
    # works for all levels of the menu
    # primary.dom_id = 'menu-id'
    # primary.dom_class = 'menu-class'

    # You can turn off auto highlighting for a specific level
    # primary.auto_highlight = false

  end

end
