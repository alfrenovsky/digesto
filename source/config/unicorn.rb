preload_app true

timeout 300

listen 3000, :tcp_nopush => true

pid "tmp/unicorn.pid"

#stderr_path "log/unicorn.stderr.log"
#stdout_path "log/unicorn.stdout.log"

before_fork do |server, worker|
  defined?(ActiveRecord::Base) and
  ActiveRecord::Base.connection.disconnect!
end

after_fork do |server, worker|
  defined?(ActiveRecord::Base) and
  ActiveRecord::Base.establish_connection
end

