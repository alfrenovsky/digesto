Digesto::Application.routes.draw do


  resources :categories

  get "backups/index"

  get "backups/new"

  get "backups/destroy"

  get "backups/show"

  get "/zm" => redirect("/regulations")

  resources :words
  resources :expressions
  resources :regulation_types
  resources :types 
  resources :issuers
  resources :institutions
  resources :backups

  resources :regulations do
    post 'mass_create', :action => :mass_create, :on => :collection, :as => :mass_create
    get 'missing', :action => :missing, :on => :collection, :as => :missing
    member do
      get 'view(/:version)', :action => :file, :disposition => "inline", :as => :view
      get 'download(/:version)', :action => :file, :disposition => "attachment", :as => :download
    end
  end

  devise_for :users
  resources :users

  get 'config' => 'sysconfigs#edit', :as => :edit_config
  post 'config' => 'sysconfigs#update', :as => :update_config
  put 'config' => 'sysconfigs#update', :as => :update_config

  match 'view/tmp/*tmp_file' => 'file#file', :as => :view_temp
  match 'view/:disposition/:class_name/:field_name/:id(/:version)'  => 'file#file', :as => :view


#  configurations GET    /configurations(.:format)                      {:controller=>"configurations", :action=>"index"}
#  POST   /configurations(.:format)                      {:controller=>"configurations", :action=>"create"}
#  new_configuration GET    /configurations/new(.:format)                  {:controller=>"configurations", :action=>"new"}
#  edit_configuration GET    /configurations/:id/edit(.:format)             {:controller=>"configurations", :action=>"edit"}
#  configuration GET    /configurations/:id(.:format)                  {:controller=>"configurations", :action=>"show"}
#  PUT    /configurations/:id(.:format)                  {:controller=>"configurations", :action=>"update"}
#  DELETE /configurations/:id(.:format)                  {:controller=>"configurations", :action=>"destroy"}

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  root :to => "regulations#index"

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id(.:format)))'
end
