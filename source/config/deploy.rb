set :application, "set your application name here"
set :repository,  "set your repository location here"
set :scm, :git
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`

set :rails_env, 'production'
set :domain, "ruby"
set :user, "digesto"
ssh_options[:port] = 22
ssh_options[:forward_agent] = true
set :deploy_to, "/home/digesto/deploy"
set :repository,  "ssh://digesto@rails/home/digesto/digesto.git"
set :repository,  "git@github.com:alfrenovsky/Digesto.git"

puts "Deployando a producción: #{domain}"

role :web, domain                          # Your HTTP server, Apache/etc
role :app, domain                          # This may be the same as your `Web` server
role :db,  domain, :primary => true # This is where Rails migrations will run

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

# If you are using Passenger mod_rails uncomment this:
namespace :deploy do
  task :start do ; end
  task :stop do ; end
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
  end
end

namespace :rails do
  desc "Symlinks the shared/config/database yaml to release/config/"
  task :symlink_db_config, :roles => :app do
    puts "Copying database configuration to release path"
    run "#{try_sudo} rm #{release_path}/config/database.yml -f"
    run "#{try_sudo} ln -s #{shared_path}/config/database.yml #{release_path}/config/database.yml"
  end
end

after "deploy:symlink", "rails:symlink_db_config"
after "deploy:symlink", "deploy:migrate"

ssh_options[:paranoid] = false
default_run_options[:pty] = true
set :deploy_via, :remote_cache
set :keep_releases, 3
set :use_sudo, false

require "bundler/capistrano"



task :link_stuff do
  %w{uploads public/uploads}.each do |share|
    run "mkdir -p \"#{shared_path}/#{share}\""
    run "ln -s \"#{shared_path}/#{share}\" \"#{release_path}/#{share}\""
  end
end

after("deploy:update_code","rails:symlink_db_config")
after("deploy:update_code","link_stuff")


#$:.unshift(File.expand_path('./lib', ENV['rvm_path']))
require "rvm/capistrano"
#set :rvm_ruby_string, 'ruby-1.9.3-p0'
set :rvm_ruby_string, 'ruby-1.9.3-p194@digesto'
set :rvm_type, :system
