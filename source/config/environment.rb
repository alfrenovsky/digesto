# Load the rails application
require File.expand_path('../application', __FILE__)

RMAGICK_BYPASS_VERSION_TEST = true

Encoding.default_external = Encoding::UTF_8
Encoding.default_internal = Encoding::UTF_8
# Initialize the rails application
Digesto::Application.initialize!

Digesto::Application.configure do
  config.encoding = "utf-8"
end
