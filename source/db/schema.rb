# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130730110854) do

  create_table "attachments", :force => true do |t|
    t.string   "name"
    t.string   "file"
    t.integer  "regulation_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "categories", :force => true do |t|
    t.string "name"
    t.text   "description"
  end

  create_table "expressions", :force => true do |t|
    t.text     "regex"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "as_string"
    t.integer  "regexable_id"
    t.string   "regexable_type"
  end

  create_table "indices", :force => true do |t|
    t.integer "word_id"
    t.integer "regulation_id"
  end

  create_table "institutions", :force => true do |t|
    t.string   "name"
    t.integer  "issuer_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "issuers", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "abbreviation"
    t.integer  "institution_id"
  end

  create_table "permissions", :force => true do |t|
    t.integer  "role_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "regulation_categories", :force => true do |t|
    t.integer "regulation_id"
    t.integer "category_id"
  end

  create_table "regulation_types", :force => true do |t|
    t.integer  "issuer_id"
    t.integer  "type_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "regulations", :force => true do |t|
    t.integer  "regulation_type_id"
    t.integer  "number"
    t.text     "description"
    t.text     "text_content"
    t.date     "date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "file"
    t.text     "abstract"
    t.boolean  "confirmation",              :default => false
    t.integer  "regulation_category_count", :default => 0
    t.boolean  "has_categories"
  end

  create_table "roles", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sysconfigs", :force => true do |t|
    t.string   "company"
    t.text     "footer"
    t.string   "default_sender"
    t.string   "smtp_address"
    t.integer  "smtp_port"
    t.string   "smtp_domain"
    t.boolean  "smtp_need_auth"
    t.string   "smtp_user_name"
    t.string   "smtp_password"
    t.string   "smtp_authentication"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "logo"
  end

  create_table "types", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "abbreviation"
  end

  create_table "users", :force => true do |t|
    t.string   "email",                                 :default => "", :null => false
    t.string   "encrypted_password",     :limit => 128, :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                         :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "password_salt"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.integer  "failed_attempts",                       :default => 0
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.string   "authentication_token"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "words", :force => true do |t|
    t.string "word"
  end

end
