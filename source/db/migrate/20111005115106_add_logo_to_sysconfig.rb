class AddLogoToSysconfig < ActiveRecord::Migration
  def self.up
    add_column :sysconfigs, :logo, :string
  end

  def self.down
    remove_column :sysconfigs, :logo
  end
end
