class DropDelimiters < ActiveRecord::Migration
  def self.up
    Expression.where(:regexable_type => "Delimiter").each do |e|
      e.update_attribute(:regexable_id,nil)
    end
    drop_table :delimiters
  end

  def self.down
    create_table :delimiters do |t|
      t.timestamps
    end
  end
end
