class Regulations < ActiveRecord::Migration
  def self.up
    add_column :regulations, :regulation_category_count, :integer, :default => 0

    Regulation.all.each do |r|
      Regulation.update_counters r.id, :regulation_category_count => r.regulation_category.length
    end
  end

  def self.down
    remove_column :regulations, :regulation_category_count
  end
end
