class CreateRegulations < ActiveRecord::Migration
  def self.up
    create_table :regulations do |t|
      t.integer :regulation_type_id
      t.integer :number
      t.string :description
      t.text :text_content
      t.date :date

      t.timestamps
    end
  end

  def self.down
    drop_table :regulations
  end
end
