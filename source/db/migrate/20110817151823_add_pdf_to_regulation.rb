class AddPdfToRegulation < ActiveRecord::Migration
  def self.up
    add_column :regulations, :file, :string
  end

  def self.down
    remove_column :regulations, :file
  end
end
