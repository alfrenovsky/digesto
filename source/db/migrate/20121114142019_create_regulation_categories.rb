class CreateRegulationCategories < ActiveRecord::Migration
  def self.up
    create_table :regulation_categories do |t|
      t.integer :regulation_id
      t.integer :category_id
    end
  end

  def self.down
    drop_table :regulation_categories
  end
end
