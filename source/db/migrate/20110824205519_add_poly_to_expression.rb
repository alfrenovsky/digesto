class AddPolyToExpression < ActiveRecord::Migration
  def self.up
    add_column :expressions, :regexable_id, :integer
    add_column :expressions, :regexable_type, :string
  end

  def self.down
    remove_column :expressions, :regexable_type
    remove_column :expressions, :regexable_id
  end
end
