class AddHasCategoriesToRegulations < ActiveRecord::Migration
  def self.up
    add_column :regulations, :has_categories, :boolean
    Regulation.all.each do |r|
      hc=(r.categories.count > 0)
      r.update_attributes( :has_categories => hc ) rescue nil
    end
  end

  def self.down
    remove_column :regulations, :has_categories
  end
end
