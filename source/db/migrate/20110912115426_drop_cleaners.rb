class DropCleaners < ActiveRecord::Migration
  def self.up
    Expression.where(:regexable_type => "Cleaner").each do |e|
      e.update_attribute(:regexable_id,nil)
    end
    drop_table :cleaners
  end

  def self.down
    create_table :cleaners do |t|
      t.timestamps
    end
  end
end
