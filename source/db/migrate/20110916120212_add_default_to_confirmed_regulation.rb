class AddDefaultToConfirmedRegulation < ActiveRecord::Migration
  def self.up
    change_column_default("regulations", "confirmation", false)
  end

  def self.down
    change_column_default("regulations", "confirmation", nil)
  end
end
