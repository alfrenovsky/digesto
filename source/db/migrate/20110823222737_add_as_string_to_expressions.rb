class AddAsStringToExpressions < ActiveRecord::Migration
  def self.up
    add_column :expressions, :as_string, :boolean
  end

  def self.down
    remove_column :expressions, :as_string
  end
end
