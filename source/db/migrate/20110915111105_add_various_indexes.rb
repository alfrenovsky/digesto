class AddVariousIndexes < ActiveRecord::Migration
  def self.up
    add_index "attachments", ["regulation_id"], :name => "regulation"
    add_index "expressions", ["regexable_type","regexable_id"], :name => "regexable"
    add_index "indices", ["regulation_id","word_id"], :name => "joint"
    add_index "institutions", ["issuer_id"], :name => "issuer"
    add_index "permissions", ["role_id","user_id"], :name => "permissions_joint"
    add_index "regulation_types", ["issuer_id","type_id"], :name => "regulation_types_joint"
    add_index "regulations", ["regulation_type_id"], :name => "regulation_type"
  end

  def self.down
    remove_index "attachments", :name => "regulation"
    remove_index "expressions", :name => "regexable"
    remove_index "indices", :name => "joint"
    remove_index "institutions", :name => "issuer"
    remove_index "permissions", :name => "permissions_joint"
    remove_index "regulation_types", :name => "regulation_types_joint"
    remove_index "regulations", :name => "regulation_type"
  end
end
