class AddFieldsToIssuer < ActiveRecord::Migration
  def self.up
    add_column :issuers, :abbreviation, :string
    add_column :issuers, :institution_id, :integer
  end

  def self.down
    remove_column :issuers, :institution_id
    remove_column :issuers, :abbreviation
  end
end
