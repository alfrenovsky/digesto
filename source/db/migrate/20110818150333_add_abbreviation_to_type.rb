class AddAbbreviationToType < ActiveRecord::Migration
  def self.up
    add_column :types, :abbreviation, :string
  end

  def self.down
    remove_column :types, :abbreviation
  end
end
