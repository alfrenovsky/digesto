class Reindex < ActiveRecord::Migration
  def self.up
    Word.transaction do
      Word.all.each{|w| w.destroy}
      Regulation.where(:confirmation => true).each do |r|
        puts r.title
        r.update_index
      end
    end
  end

  def self.down
  end
end
