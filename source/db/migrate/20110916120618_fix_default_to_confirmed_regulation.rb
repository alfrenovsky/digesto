class FixDefaultToConfirmedRegulation < ActiveRecord::Migration
  def self.up
    Regulation.where(:confirmation => nil).each do |r|
      r.confirmation=false
      r.save(:validate => false)
    end
  end

  def self.down
  end
end
