class CreateSysconfigs < ActiveRecord::Migration
  def self.up
    create_table :sysconfigs do |t|
      t.string :company
      t.text :footer
      t.string :default_sender
      t.string :smtp_address
      t.integer :smtp_port
      t.string :smtp_domain
      t.boolean :smtp_need_auth
      t.string :smtp_user_name
      t.string :smtp_password
      t.string :smtp_authentication

      t.timestamps
    end
  end

  def self.down
    drop_table :sysconfigs
  end
end
