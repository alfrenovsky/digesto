class CreateDelimiters < ActiveRecord::Migration
  def self.up
    create_table :delimiters do |t|
      t.boolean :starter

      t.timestamps
    end
  end

  def self.down
    drop_table :delimiters
  end
end
