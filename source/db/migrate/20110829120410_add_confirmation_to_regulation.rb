class AddConfirmationToRegulation < ActiveRecord::Migration
  def self.up
    add_column :regulations, :confirmation, :boolean
  end

  def self.down
    remove_column :regulations, :confirmation
  end
end
