class ReindexAgain3 < ActiveRecord::Migration
  def self.up
    Word.transaction do
      Word.all.each{|w| w.destroy}
    end
    Word.transaction do
      Regulation.all.each do |r|
        puts r.title
        r.update_index
      end
    end
  end

  def self.down
  end
end
