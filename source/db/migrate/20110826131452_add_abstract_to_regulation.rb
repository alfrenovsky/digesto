class AddAbstractToRegulation < ActiveRecord::Migration
  def self.up
    add_column :regulations, :abstract, :text
  end

  def self.down
    remove_column :regulations, :abstract
  end
end
