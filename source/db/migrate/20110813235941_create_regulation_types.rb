class CreateRegulationTypes < ActiveRecord::Migration
  def self.up
    create_table :regulation_types do |t|
      t.integer :issuer_id
      t.integer :type_id

      t.timestamps
    end
  end

  def self.down
    drop_table :regulation_types
  end
end
