class RemoveDatesFromIndices < ActiveRecord::Migration
  def self.up
    remove_column :indices, :created_at
    remove_column :indices, :updated_at
  end

  def self.down
    add_column :indices, :updated_at, :datetime
    add_column :indices, :created_at, :datetime
  end
end
