class ChangeDescriptionTypeInRegulations < ActiveRecord::Migration
  def self.up
    change_column :regulations, :description, :text
  end

  def self.down
    change_column :regulations, :description, :string
  end
end
