// Place your application-specific JavaScript functions and classes here
// This file is automatically included by javascript_include_tag :defaults
$(function(){ $(".autocomplete").chosen({allow_single_deselect:true}); });



$(function(){ $(".elastic").elastic(); });

function remove_fields(link) {
				$(link).prev("input[type=hidden]").val("1");
				$(link).closest(".fields").hide();
}

function add_fields(link, association, content) {
				var new_id = new Date().getTime();
				var regexp = new RegExp("new_" + association, "g");
				$(link).parent().before(content.replace(regexp, new_id));
				init_fields($(link).parent().prev());  		
}


$(function(){
	$(".flash").delay(3000).fadeOut(500);
	$(".flash").click(function() { $(this).hide() });
});


$(function(){
   $("#order_updated_at").change( function(){ $(this).closest("form").submit(); });
   $("#order_date").change( function(){ $(this).closest("form").submit(); });
 });

